package gaoyun.com.owm_client.web.service;

import android.app.Service;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by artem on 06.02.2018.
 */

public class SearchCityService extends BaseService {

    String cityPath;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getListCities();

        cityPath = intent.getStringExtra("cityPath");

        return Service.START_NOT_STICKY;
    }

    private void getListCities(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                String url = "http://api.openweathermap.org/data/2.5/find?mode=json&type=like&q=";
                String resultsCount = "&cnt=5&appid=ca765277181e3f62a4be2e093ea04ced";
                HashMap<String, String> cityMap = new HashMap<>();

                try {
                    String query = url.concat(cityPath).concat(resultsCount);
                    JSONObject jObj = new JSONObject(new BaseServerAnswer().download(query));
                    JSONArray jArr = jObj.getJSONArray("list");

                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject obj = jArr.getJSONObject(i);

                        String name = obj.getString("name");
                        String id = obj.getString("id");
                        cityMap.put(id, name);

                    }

                    //Выкидываем в активность
                    Intent cityListIntent = new Intent(BaseService.SERVICE_RESULT);
                    cityListIntent.putExtra(BaseService.SERVICE_ACTIVITY, "AddCityActivity");
                    cityListIntent.putExtra(BaseService.SERVICE_OPERATION, 1);
                    cityListIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    cityListIntent.putExtra("cityList", cityMap);
                    localBroadcastManager.sendBroadcast(cityListIntent);

                }catch (JSONException jsone){
                    jsone.printStackTrace();
                }catch (IOException ioe){
                    ioe.printStackTrace();
                }

            }
        }).start();
    }
}

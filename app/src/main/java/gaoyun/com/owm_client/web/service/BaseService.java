package gaoyun.com.owm_client.web.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;


public abstract class BaseService extends Service {
    protected String url;
    protected static LocalBroadcastManager localBroadcastManager;

    public static final String SERVICE_RESULT = "com.gaoyun.cube.SERVICE_RESULT";
    public static final String SERVICE_ACTIVITY = "com.gaoyun.cube.SERVICE_ACTIVITY";
    public static final String SERVICE_OPERATION = "com.gaoyun.cube.SERVICE_OPERATION";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }
}

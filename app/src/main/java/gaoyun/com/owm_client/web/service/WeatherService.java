package gaoyun.com.owm_client.web.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import gaoyun.com.owm_client.web.model.CityItem;

/**
 * Created by artem on 06.02.2018.
 */

public class WeatherService extends BaseService {

    ArrayList<String> cityIds;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        cityIds = new ArrayList<>();
        cityIds = intent.getStringArrayListExtra("cityIds");

        getWeather();

        return Service.START_NOT_STICKY;
    }

    private void getWeather() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                String url = "http://api.openweathermap.org/data/2.5/group?id=";
                String units = "&units=metric&appid=ca765277181e3f62a4be2e093ea04ced";

                ArrayList<CityItem> cityWeathers = new ArrayList<>();

                try {
                    for(String id: cityIds) {
                        if((cityIds.indexOf(id)<cityIds.size()-1)&&(cityIds.size()>1))
                            url = url.concat(id+",");
                        else url = url.concat(id);
                    }
                    url = url.concat(units);

                    JSONObject jObj = new JSONObject(new BaseServerAnswer().download(url));
                    JSONArray jArr = jObj.getJSONArray("list");

                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject obj = jArr.getJSONObject(i);

                        JSONObject weather = obj.getJSONArray("weather").getJSONObject(0);
                        JSONObject main = obj.getJSONObject("main");
                        JSONObject wind = obj.getJSONObject("wind");

                        String name = obj.getString("name");
                        String weatherState = weather.getString("main");
                        int temp = main.getInt("temp");
                        int windSpeed = wind.getInt("speed");
                        int windDirection = -1;
                        try{
                            windDirection = wind.getInt("deg");
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        CityItem city = new CityItem(name, cityIds.get(i), weatherState, temp, windSpeed, windDirection);
                        cityWeathers.add(city);

                    }

                    //Выкидываем в активность
                    Intent cityListIntent = new Intent(BaseService.SERVICE_RESULT);
                    cityListIntent.putExtra(BaseService.SERVICE_ACTIVITY, "MainActivity");
                    cityListIntent.putExtra(BaseService.SERVICE_OPERATION, 1);
                    cityListIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    cityListIntent.putExtra("cityWeathers", cityWeathers);
                    localBroadcastManager.sendBroadcast(cityListIntent);

                }catch (JSONException jsone){
                    jsone.printStackTrace();
                }catch (IOException ioe){
                    ioe.printStackTrace();

                    Intent cityListIntent = new Intent(BaseService.SERVICE_RESULT);
                    cityListIntent.putExtra(BaseService.SERVICE_ACTIVITY, "MainActivity");
                    cityListIntent.putExtra(BaseService.SERVICE_OPERATION, 0);
                    cityListIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    localBroadcastManager.sendBroadcast(cityListIntent);

                }

            }
        }).start();
    }
}

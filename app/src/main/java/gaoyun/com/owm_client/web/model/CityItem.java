package gaoyun.com.owm_client.web.model;

/**
 * Created by artem on 06.02.2018.
 */

public class CityItem {
    private String name;
    private String code;
    private String weatherState;
    private int temp;
    private int windSpeed;
    private int windDirection;

    public CityItem(String name){
        this.name = name;
    }

    public CityItem(String name, String code, String weatherState, int temp, int windSpeed, int windDirection){
        this.name = name;
        this.code = code;
        this.weatherState = weatherState;
        this.temp = temp;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getWeatherState() {
        return weatherState;
    }

    public void setWeatherState(String weatherState) {
        this.weatherState = weatherState;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(int windDirection) {
        this.windDirection = windDirection;
    }
}

package gaoyun.com.owm_client.web.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by artem on 16.12.2017.
 */

public class BaseServerAnswer {

    public String download(String myUrl) throws IOException {
        InputStream is = null;
        String line;

        try {
            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            is = conn.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder buf = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            line = buf.toString();
        } finally {
            if (is != null) {
                is.close();
            }
        }

        return line;
    }
}

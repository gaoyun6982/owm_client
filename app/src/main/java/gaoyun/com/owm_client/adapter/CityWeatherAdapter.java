package gaoyun.com.owm_client.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import gaoyun.com.owm_client.Constants;
import gaoyun.com.owm_client.R;
import gaoyun.com.owm_client.web.model.CityItem;

/**
 * Created by artem on 06.02.2018.
 */

public class CityWeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;

    ArrayList<CityItem> listItems;
    Context context;

    public CityWeatherAdapter(ArrayList<CityItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_city_card, parent, false);
        return new VHItem(v);
    }

    private CityItem getItem(int position) {
        return listItems.get(position);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final CityItem currentItem = getItem(position);
        final VHItem VHitem = (VHItem) holder;
        try {
            VHitem.weatherStateImage.setImageDrawable(ContextCompat.getDrawable(context, getWeatherImage(currentItem.getWeatherState())));
            VHitem.cityNameText.setText(currentItem.getName());
            VHitem.temperatureText.setText(String.valueOf(currentItem.getTemp()).concat(context.getResources().getString(R.string.celsius)));
            VHitem.windSpeedText.setText(String.valueOf(currentItem.getWindSpeed()).concat(context.getResources().getString(R.string.meters_per_second)));
            VHitem.windDirectionText.setText(getWindDirectionFromDegree(currentItem.getWindDirection()));

            VHitem.cityWeatherCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    MaterialDialog.Builder materialDialogMessage = new MaterialDialog.Builder(context);
                    materialDialogMessage.positiveText("Yes");
                    materialDialogMessage.negativeText("No");
                    materialDialogMessage.contentColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
                    materialDialogMessage.positiveColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
                    materialDialogMessage.negativeColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
                    materialDialogMessage.backgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
                    materialDialogMessage.content("Are you want to delete this city?");
                    final MaterialDialog dialogMessage = materialDialogMessage.build();
                    materialDialogMessage.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            ArrayList<String> ids = getStringArrayPref(context, Constants.IDS);
                            ArrayList<String> cities = getStringArrayPref(context, Constants.CITIES);

                            cities.remove(currentItem.getName());
                            ids.remove(currentItem.getCode());

                            setStringArrayPref(context, Constants.IDS, ids);
                            setStringArrayPref(context, Constants.CITIES, cities);
                            listItems.remove(currentItem);
                            notifyDataSetChanged();

                            dialogMessage.dismiss();
                        }
                    });
                    materialDialogMessage.onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialogMessage.dismiss();
                        }
                    });
                    materialDialogMessage.show();
                    return false;
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class VHItem extends RecyclerView.ViewHolder {

        ImageView weatherStateImage;
        TextView cityNameText;
        TextView temperatureText;
        TextView windSpeedText;
        TextView windDirectionText;
        CardView cityWeatherCard;

        public VHItem(View itemView) {
            super(itemView);

            weatherStateImage = (ImageView) itemView.findViewById(R.id.weatherStateImage);
            cityNameText = (TextView) itemView.findViewById(R.id.cityNameText);
            temperatureText = (TextView) itemView.findViewById(R.id.temperatureText);
            windSpeedText = (TextView) itemView.findViewById(R.id.windSpeedText);
            windDirectionText = (TextView) itemView.findViewById(R.id.windDirectionText);
            cityWeatherCard = (CardView) itemView.findViewById(R.id.cityWeatherCard);

        }
    }

    public ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.commit();
    }

    private int getWeatherImage(String weather){
        SimpleDateFormat time = new SimpleDateFormat("H");
        long currentTime = System.currentTimeMillis();

        Integer hour = Integer.valueOf(time.format(currentTime));
        switch (weather){
            case("Clear"):{
                if((hour > 22) || (hour < 5)) return R.drawable.night;
                else return R.drawable.sun;
            }
            case("Clouds"):{
                if((hour > 22) || (hour < 5)) return R.drawable.cloudly_night;
                else return R.drawable.cloudly_day;
            }
            case("Drizzle"):
            case("Rain"):{
                return R.drawable.rain;
            }
            case("Thunderstorm"):{
                return R.drawable.thunderstorm;
            }
            case("Atmosphere"):
            case("Mist"):{
                return R.drawable.fog;
            }
            case("Snow"):{
                return R.drawable.snow;
            }
        }
        return 0;
    }

    private String getWindDirectionFromDegree(int deg){
        String windVector = "";
        if(deg>0) {
            if ((deg >= 340) && (deg <= 20))
                windVector = "N";
            else if ((deg > 20) && (deg < 70))
                windVector = "NE";
            else if ((deg >= 70) && (deg <= 110))
                windVector = "E";
            else if ((deg > 110) && (deg < 160))
                windVector = "SE";
            else if ((deg >= 160) && (deg <= 200))
                windVector = "S";
            else if ((deg > 200) && (deg < 250))
                windVector = "SW";
            else if ((deg >= 250) && (deg <= 290))
                windVector = "W";
            else windVector = "NW";
        }else {
            windVector = "";
        }

        return windVector;
    }

}

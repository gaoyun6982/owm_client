package gaoyun.com.owm_client.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import gaoyun.com.owm_client.Constants;
import gaoyun.com.owm_client.R;
import gaoyun.com.owm_client.web.service.BaseService;
import gaoyun.com.owm_client.web.service.SearchCityService;

public class AddCityActivity extends BaseSwipeBackActivity {

    AutoCompleteTextView searchText;
    Button searchButton;

    String[] cities = {""};
    String choosedCity = "";

    HashMap<String, String> hm;

    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver broadcastReceiver;

    int result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        searchText = (AutoCompleteTextView) findViewById(R.id.searchText);
        searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setEnabled(false);

        searchText.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, cities));

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {

                    searchButton.setEnabled(false);
                    choosedCity = "";

                    Intent searchCities = new Intent(AddCityActivity.this, SearchCityService.class);
                    searchCities.putExtra("cityPath", String.valueOf(s));
                    startService(searchCities);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                choosedCity = searchText.getText().toString();
                searchText.dismissDropDown();
                searchButton.setEnabled(true);
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> cityElements = getStringArrayPref(getApplicationContext(), Constants.CITIES);
                ArrayList<String> idsElements = getStringArrayPref(getApplicationContext(), Constants.IDS);

                if(cityElements == null){
                    ArrayList<String> cityElementsSet = new ArrayList<>();
                    cityElementsSet.add(searchText.getText().toString());
                    setStringArrayPref(getApplicationContext(), Constants.CITIES, cityElementsSet);
                }else{
                    cityElements.add(searchText.getText().toString());
                    setStringArrayPref(getApplicationContext(), Constants.CITIES, cityElements);
                }

                if(idsElements == null) {
                    ArrayList<String> idsElementsSet = new ArrayList<>();
                    idsElementsSet.add(getKeyFromValue(hm, searchText.getText().toString()));
                    setStringArrayPref(getApplicationContext(), Constants.IDS, idsElementsSet);

                }else{
                    idsElements.add(getKeyFromValue(hm, searchText.getText().toString()));
                    setStringArrayPref(getApplicationContext(), Constants.IDS, idsElements);
                }
                result = RESULT_OK;
                Toast.makeText(AddCityActivity.this, "Added city " + searchText.getText().toString(), Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra(BaseService.SERVICE_ACTIVITY).equals("AddCityActivity")) {
                    switch (intent.getIntExtra(BaseService.SERVICE_OPERATION, 0)) {
                        case 1: {

                            hm = (HashMap<String, String>) intent.getSerializableExtra("cityList");
                            Object[] objects = hm.values().toArray();
                            cities = new String[objects.length];
                            for(int i = 0; i < objects.length; i++) cities[i] = String.valueOf(objects[i]);

                            if ((cities.length > 0)&&(!searchText.getText().toString().equals(choosedCity))) {
                                setAdapterCities(cities);
                            }
                            break;
                        }
                    }
                }
            }
        };

    }

    public String getKeyFromValue(Map hm, String value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return String.valueOf(o);
            }
        }
        return null;
    }

    public ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver), new IntentFilter(BaseService.SERVICE_RESULT));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        setResult(result);
        super.onBackPressed();
        this.overridePendingTransition(R.anim.dealpha, R.anim.bottom_out);
    }

    public void setAdapterCities(String[] adapter) {
        searchText.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, adapter));
        searchText.showDropDown();
    }
}

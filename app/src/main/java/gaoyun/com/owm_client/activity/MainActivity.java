package gaoyun.com.owm_client.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import gaoyun.com.owm_client.Constants;
import gaoyun.com.owm_client.adapter.CityWeatherAdapter;
import gaoyun.com.owm_client.R;
import gaoyun.com.owm_client.web.model.CityItem;
import gaoyun.com.owm_client.web.service.BaseService;
import gaoyun.com.owm_client.web.service.WeatherService;

public class MainActivity extends AppCompatActivity {

    RecyclerView cityRecyclerView;
    FloatingActionButton citiesButton;
    ProgressBar progressBar;
    SwipeRefreshLayout mainRefresh;

    ArrayList<CityItem> cities;
    ArrayList<String> cityElements;

    CityWeatherAdapter cityWeatherAdapter;

    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityRecyclerView = (RecyclerView) findViewById(R.id.cityRecyclerView);
        citiesButton = (FloatingActionButton) findViewById(R.id.citiesButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mainRefresh = (SwipeRefreshLayout) findViewById(R.id.mainRefresh);

        citiesButton.setVisibility(View.INVISIBLE);
        citiesButton.hide();
        progressBar.setVisibility(View.VISIBLE);
        mainRefresh.setEnabled(false);

        mainRefresh.setProgressBackgroundColorSchemeResource(R.color.colorWhite);
        mainRefresh.setColorSchemeResources(R.color.colorAccent);
        mainRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    cityElements = getStringArrayPref(getApplicationContext(), Constants.IDS);

                    Intent searchCities = new Intent(MainActivity.this, WeatherService.class);
                    searchCities.putExtra("cityIds", cityElements);
                    startService(searchCities);
                }catch (NullPointerException npe){
                    npe.printStackTrace();
                }
            }
        });

        cities = new ArrayList<>();
        try {
            cityElements = getStringArrayPref(getApplicationContext(), Constants.IDS);

            Intent searchCities = new Intent(MainActivity.this, WeatherService.class);
            searchCities.putExtra("cityIds", cityElements);
            startService(searchCities);
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }

        RecyclerView.LayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        cityRecyclerView.setLayoutManager(mLayoutManager);

        cityRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy){
            if (dy<0 && !citiesButton.isShown())
                citiesButton.show();
            else if(dy>0 && citiesButton.isShown())
                citiesButton.hide();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    });

        citiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toCitiesIntent = new Intent(MainActivity.this, AddCityActivity.class);
                startActivityForResult(toCitiesIntent, Constants.REQUEST_ADD_CITY);
                MainActivity.this.overridePendingTransition(R.anim.bottom_in, R.anim.alpha);
            }
        });

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra(BaseService.SERVICE_ACTIVITY).equals("MainActivity")) {
                    switch (intent.getIntExtra(BaseService.SERVICE_OPERATION, 0)) {
                        case 0:{
                            citiesButton.setVisibility(View.VISIBLE);
                            citiesButton.show();
                            progressBar.setVisibility(View.GONE);
                            mainRefresh.setEnabled(true);

                            Toast.makeText(MainActivity.this, "Add city.", Toast.LENGTH_SHORT).show();

                            Intent toCitiesIntent = new Intent(MainActivity.this, AddCityActivity.class);
                            startActivityForResult(toCitiesIntent, Constants.REQUEST_ADD_CITY);
                            MainActivity.this.overridePendingTransition(R.anim.bottom_in, R.anim.alpha);

                            break;
                        }
                        case 1: {
                            cities = (ArrayList<CityItem>) intent.getSerializableExtra("cityWeathers");
                            cityWeatherAdapter = new CityWeatherAdapter(cities, MainActivity.this);
                            cityRecyclerView.setAdapter(cityWeatherAdapter);

                            ArrayList<String> citiesSet = new ArrayList<>();
                            ArrayList<String> idsSet = new ArrayList<>();
                            for(CityItem city: cities){
                                citiesSet.add(city.getName());
                                idsSet.add(city.getCode());
                            }

                            setStringArrayPref(getApplicationContext(), Constants.CITIES, citiesSet);
                            setStringArrayPref(getApplicationContext(), Constants.IDS, idsSet);

                            citiesButton.setVisibility(View.VISIBLE);
                            citiesButton.show();
                            progressBar.setVisibility(View.GONE);
                            mainRefresh.setRefreshing(false);
                            mainRefresh.setEnabled(true);

                            break;
                        }
                    }
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == Constants.REQUEST_ADD_CITY)&&(resultCode == RESULT_OK)){
            cityElements = getStringArrayPref(getApplicationContext(), Constants.IDS);
            ArrayList<String> cityIds = new ArrayList<>();
            cities.clear();
            for (Object cityElement : cityElements) {
                cities.add(new CityItem(String.valueOf(cityElement)));
                cityIds.add(String.valueOf(cityElement));
            }

            Intent searchCities = new Intent(MainActivity.this, WeatherService.class);
            searchCities.putExtra("cityIds", cityIds);
            startService(searchCities);
        }
        else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver), new IntentFilter(BaseService.SERVICE_RESULT));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}
